/**
 * @typedef { import('../api/countries.d.ts').Country } Country
 */

/**
 * Don't do this in your production library.
 * Instead, wrap this whole module in a function 
 * and return the class as an anonymous class.
 * 
 * We have to fetch the stylesheet outside of the class to
 * avoid refetch resources multiple time. It doesn't look good
 * in Network tab.
 */
const default_css = fetch('./lib/ce-card.css').then(res => res.text())

export class Card extends HTMLElement
{
   static observedAttributes = ['alpha3', 'name', 'population', 'region', 'capital', 'flag']

   /** @type { Partial<Country> } */
   #attr = {
      name: 'unknown',
      population: '?',
      region: '?',
      capital: '?',
      flag: undefined,
      alpha3Code: undefined
   }

   #root = this.attachShadow({ mode: 'closed' });

   constructor()
   {
      // https://html.spec.whatwg.org/multipage/custom-elements.html#custom-element-conformance
      super()

      this.#inject_style()
   }

   async #inject_style()
   {
      /** @type { HTMLStyleElement } */
      const style = document.createElement('style')
      style.textContent = await default_css
      this.#root.appendChild(style)
   }


   attributeChangedCallback(name, _oldval, newval)
   {
      if (['undefined', 'null', 'false', 'NaN'].includes(newval))
      {
         console.log(name, newval, typeof newval)
         newval = undefined
      }

      switch (name)
      {
         case 'alpha3':
            this.#attr.alpha3Code = newval
            break
         case 'flag':
            this.#attr.flag = URL.canParse(newval) ? new URL(newval) : undefined
            this.#render_flag()
            break
         case 'name':
            this.#attr.name = newval || 'unknown'
            this.#render_name()
            break
         case 'population':
            this.#attr.population = parseFloat(newval) || '?'
            this.#render_dd_content(
               name,
               this.#attr.population?.toLocaleString()
            )
            break
         default:
            this.#attr[name] = newval || '?'
            this.#render_dd_content(name)
      }
   }

   connectedCallback()
   {
      // disable is-loading state
      this.removeAttribute('is-loading')

      // support Accessibility
      this.setAttribute('role', 'article')
      this.setAttribute('aria-label', `Country flag of ${this.#attr.name}`)

      // attr: Flag
      const flag = this.#create_flag()
      this.#root.appendChild(flag)

      // attr: Name
      const name = document.createElement('h1')
      name.innerText = this.#attr.name
      this.#root.appendChild(name)

      // attr: Population, Region, Capital
      const dl_content = this.#compose_dl_content()
      if (dl_content)
      {
         const info_ls = document.createElement('dl')
         info_ls.innerHTML = dl_content
         this.#root.appendChild(info_ls)
      }
   }

   // disconnectedCallback() { }

   // adoptedCallback() { }

   getAttribute(name)
   {
      return this.#attr[name.toLowerCase()]
   }

   get alpha3()
   {
      return this.#attr.alpha3Code
   }

   #create_flag()
   {
      const wrapper = document.createElement('figure')
      wrapper.classList.add('flag')
      this.#attr.flag || wrapper.classList.add('placeholder')

      const flag = document.createElement('img')
      flag.setAttribute('loading', 'lazy')
      flag.src = this.#attr.flag?.toString()

      wrapper.appendChild(flag)
      return wrapper
   }

   #compose_dl_content()
   {
      const item = (key, val) => `<div id='${key.toLowerCase()}'><dt>${key}</dt><dd>${val}</dd></div>`

      const { population, region, capital } = this.#attr
      const ls = [
         item('Population', population?.toLocaleString()),
         item('Region', region),
         item('Capital', capital)
      ].filter(tag => tag)

      return ls.length === 0 ? false : ls.join('')
   }

   #render_flag()
   {
      const wrapper = this.#root.querySelector('.flag')
      if (!wrapper) return void 0

      this.#attr.flag
         ? wrapper.classList.remove('placeholder')
         : wrapper.classList.add('placeholder')

      wrapper.querySelector('img').src = this.#attr.flag.toString()
   }

   #render_name()
   {
      const el = this.#root.querySelector('h1')
      if (!el) return void 0
      el.innerText = this.#attr.name
   }

   #render_dd_content(id, val = undefined)
   {
      const dd = this.#root.querySelector(`#${id} > dd`)
      if (!dd) return void 0
      dd.innerText = val || this.#attr[id]
   }

   /**
    * Custom method to quick loading data
    * @param { Country }
    */
   load_data({ name, population, region, capital, flag, alpha3Code })
   {
      this.setAttribute('name', name)
      this.setAttribute('population', population)
      this.setAttribute('region', region)
      this.setAttribute('capital', capital)

      URL.canParse(flag) && this.setAttribute('flag', flag)

      this.setAttribute('alpha3', alpha3Code)

      // support Accessibility
      this.setAttribute('role', 'article')
      this.setAttribute('aria-label', `Country flag of ${this.#attr.name}`)

      return this
   }
}

// DEMO
// customElements.define('ndt-card', Card)
