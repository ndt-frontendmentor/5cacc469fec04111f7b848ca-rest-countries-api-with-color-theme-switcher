export type HttpPattern = `https://${string}`

export type Alpha2Code = string
export type Alpha3Code = string

export type NumericStr = `${number}`

export interface Currency
{
   code: string
   name: string
   symbol: string
}

export interface Language
{
   iso639_1: string
   iso639_2: string
   name: string
   nativeName: string
}

export interface RegionalBlocs
{
   acronym: string
   name: string
   otherNames: string[]
}

export interface Country
{
   name: string
   topLevelDomain: `.${string}`[]
   alpha2Code: Alpha2Code
   alpha3Code: Alpha3Code
   callingCodes: NumericStr[]
   capital: string
   altSpellings: string[]
   subregion: string
   region: string
   population: number
   latlng: [number, number]
   demonym: string
   area: number
   gini?: number
   timezones: `UTC${"+" | "-"}${number}:${number}`[]
   borders: Alpha3Code[]
   nativeName: string
   numericCode: NumericStr
   currencies: Currency[]
   languages: Language[]
   translations: Record<Alpha2Code, string>
   flag: HttpPattern
   flags: {
      svg: HttpPattern
      png: HttpPattern
   }
   regionalBlocs: RegionalBlocs[]
   cioc?: string
   independent: boolean
}
