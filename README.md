# Frontend Mentor - REST Countries API with color theme switcher solution

[REST Countries API with color theme switcher challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/rest-countries-api-with-color-theme-switcher-5cacc469fec04111f7b848ca)

## Table of contents

- [Overview](#overview)
  - [TODO](#todo)
  - [Links](#links)
  - [Screenshots](#screenshots)
- [Study Cases](#study-cases)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
- [Author](#author)
- [Acknowledgments](#acknowledgments)
  - [Useful Resources](#useful-resources)

## Overview

**Estimated Time**: ?h & ~?min

21:00 23/01

### TODO

Users should be able to:

- [ ] See all countries from the API on the homepage
- [ ] Search for a country using an `input` field
- [ ] Filter countries by region
- [ ] Click on a country to see more detailed information on a separate page
- [ ] Click through to the border countries on the detail page
- [ ] Toggle the color scheme between light and dark mode

### Links

- [Solution URL](https://gitlab.com/ndt-frontendmentor/5cacc469fec04111f7b848ca-rest-countries-api-with-color-theme-switcher)
- [Live Site URL](https://ndt-frontendmentor.gitlab.io/5cacc469fec04111f7b848ca-rest-countries-api-with-color-theme-switcher)

### Development Notes

- To test in local, please any localhost app. For me, I'm comfortable with
  `wrangler`:
  ```shell
  cd public/
  wrangler pages dev .
  ```

### Screenshots

![](./screenshot/desktop.jpg) ![](./screenshot/desktop-dark.jpg)
![](./screenshot/mobile.jpg) ![](./screenshot/mobile-dark.jpg)

## Study Cases

### Built with

- HTML5/CSS3/Js
- Mobile-first workflow
- PWA
  ([Progressive Web App](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps))
- Web Component / Custom Element

### What I learned

<!-- TODO update later -->

## Author

- Website - [Nguyễn Đăng Tú](https://ngdangtu.com)
- Frontend Mentor -
  [@ngdangtu-vn](https://www.frontendmentor.io/profile/ngdangtu-vn)\
  (Even though I didn't wish to choose this username! There is nothing I can do
  about this 😭)
- Mastodon - [@ngdangtu@tilde.zone](https://tilde.zone/@ngdangtu)

## Acknowledgments

This is where I give a hat tip to anyone helps me or any project that backs my
work.

### Useful Resources

- Flags: https://flagpedia.net
- Icon set: https://ionic.io/ionicons
